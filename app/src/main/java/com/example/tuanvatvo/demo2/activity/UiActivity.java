package com.example.tuanvatvo.demo2.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tuanvatvo.demo2.R;
import com.example.tuanvatvo.demo2.common.CheckConnectiom;
import com.example.tuanvatvo.demo2.common.LinkURL;
import com.example.tuanvatvo.demo2.model.UserLogIn;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UiActivity extends AppCompatActivity {

    Button btn_sign_in;


    private Handler myHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui);

            myHandler.postDelayed(checkConnection,2000);

            addControls();
            addEvents();



    }

    private Runnable checkConnection = new Runnable() {
        @Override
        public void run() {
            if(CheckConnectiom.checkCon(getApplicationContext()) == false){

                AlertDialog.Builder builder = new AlertDialog.Builder(UiActivity.this);
                builder.setTitle("Không có kết nối internet !");
                builder.setCancelable(false);
                builder.setPositiveButton("Thoát", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        UiActivity.this.finish();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }else
            myHandler.postDelayed(checkConnection,2000);

        }
    };


    private void addControls() {
        btn_sign_in = findViewById(R.id.btn_sign_in);

    }
    private void  addEvents(){
        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UiActivity.this,TrangChuActivity.class);
                startActivity(intent);
                UiActivity.this.finish();

            }
        });
    }



}
