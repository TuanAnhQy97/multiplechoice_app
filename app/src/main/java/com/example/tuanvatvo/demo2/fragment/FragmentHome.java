package com.example.tuanvatvo.demo2.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tuanvatvo.demo2.R;
import com.example.tuanvatvo.demo2.adapter.SubjectAdapter;
import com.example.tuanvatvo.demo2.model.MySubject;

import java.util.ArrayList;


public class FragmentHome extends Fragment {
    RecyclerView recyclerView_subject;
    ArrayList<MySubject> listSubject;
    SubjectAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home,container,false);


        init(view);


        return  view;
    }


    private void init(View view) {
        recyclerView_subject = view.findViewById(R.id.recyclerView_subject);
        recyclerView_subject.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView_subject.setHasFixedSize(true);
        listSubject = new ArrayList<MySubject>();
        listSubject.add(new MySubject(R.drawable.image_1,getString(R.string.calculus),10));
        listSubject.add(new MySubject(R.drawable.image_2,getString(R.string.physic),11));
        listSubject.add(new MySubject(R.drawable.image_3,getString(R.string.circuit1),12));
        listSubject.add(new MySubject(R.drawable.image_4,getString(R.string.circuit2),13));
        adapter = new SubjectAdapter(getActivity(),listSubject);
        recyclerView_subject.setAdapter(adapter);
    }



}
