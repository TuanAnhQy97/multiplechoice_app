package com.example.tuanvatvo.demo2.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.tuanvatvo.demo2.R;
import com.example.tuanvatvo.demo2.activity.DeThiActivity;
import com.example.tuanvatvo.demo2.model.MySubject;

import java.util.ArrayList;


public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.MyViewHolder> {
        Activity context;
        ArrayList<MySubject> listSubbject;

    public SubjectAdapter(Activity context, ArrayList<MySubject> listSubbject) {
        this.context = context;
        this.listSubbject = listSubbject;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_subject,parent,false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final MySubject msubject = listSubbject.get(position);
        holder.img_subject.setImageResource(msubject.getImage());
        holder.txt_namesubject.setText(msubject.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DeThiActivity.class);
                intent.putExtra("lop",msubject.getLop());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listSubbject.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView img_subject;
        TextView txt_namesubject;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img_subject = itemView.findViewById(R.id.img_subject);
            txt_namesubject = itemView.findViewById(R.id.txt_namesubject);
        }
    }
}
