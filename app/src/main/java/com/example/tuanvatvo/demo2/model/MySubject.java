package com.example.tuanvatvo.demo2.model;

import java.io.Serializable;

public class MySubject implements Serializable {
    private int image;
    private String name;
    int lop;

    public MySubject(int image, String name, int lop) {
        this.image = image;
        this.name = name;
        this.lop = lop;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLop() {
        return lop;
    }

    public void setLop(int lop) {
        this.lop = lop;
    }
}
