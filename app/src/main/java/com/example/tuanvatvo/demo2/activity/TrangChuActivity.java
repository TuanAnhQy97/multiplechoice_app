package com.example.tuanvatvo.demo2.activity;

import android.content.DialogInterface;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.tuanvatvo.demo2.R;
import com.example.tuanvatvo.demo2.adapter.ViewPagerAdapter;
import com.example.tuanvatvo.demo2.common.CheckConnectiom;
import com.example.tuanvatvo.demo2.fragment.FragmentHome;
import com.example.tuanvatvo.demo2.fragment.FragmentAccount;

public class TrangChuActivity extends AppCompatActivity {

    BottomNavigationView botton_nav;
    ViewPager mViewpager;
    ViewPagerAdapter adapter;

    private Handler myHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trang_chu);

        myHandler.postDelayed(checkConnection,5000);


        addControls();
        addEvents();




    }
    private Runnable checkConnection = new Runnable() {
        @Override
        public void run() {
            if(CheckConnectiom.checkCon(getApplicationContext()) == false){
                AlertDialog.Builder builder = new AlertDialog.Builder(TrangChuActivity.this);
                builder.setTitle("Không có kết nối internet!");
                builder.setCancelable(false);
                builder.setPositiveButton("Thoát", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TrangChuActivity.this.finish();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            else
            myHandler.postDelayed(checkConnection,5000);

        }
    };

    private void addControls() {
        botton_nav = findViewById(R.id.botton_nav);
        mViewpager = findViewById(R.id.mViewpager);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentHome(), "Home");
        adapter.addFragment(new FragmentAccount(), "Account");
        mViewpager.setAdapter(adapter);
        mViewpager.setOffscreenPageLimit(2);
        mViewpager.setCurrentItem(0, false);

    }

    private void addEvents() {
        botton_nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_ic_home:
                        mViewpager.setCurrentItem(0, false);
                        break;
                    case R.id.nav_ic_account:
                        mViewpager.setCurrentItem(1, false);
                        break;
                }
                return true;
            }
        });
        botton_nav.setSelectedItemId(R.id.nav_ic_home);
    }
}
